ARG CADDY_VERSION=2.8.4
FROM caddy:${CADDY_VERSION}-builder AS builder

RUN xcaddy build v2.8.4 \
    --with github.com/lucaslorentz/caddy-docker-proxy/v2@v2.9.1 \
    --with github.com/caddy-dns/cloudflare
    --with github.com/caddy-dns/cloudflare
    --with github.com/greenpau/caddy-security@v1.1.29 \
    --with caddy-security-secrets-static-secrets-manager@latest \
    --with github.com/greenpau/caddy-trace@latest \

FROM caddy:${CADDY_VERSION}

COPY --from=builder /usr/bin/caddy /usr/bin/caddy

CMD ["caddy", "docker-proxy"]
